# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
bases:
  - "../../bases/environments.yaml"
---
repositories:
  # openDesk Otterize
  # Source: https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize
  - name: "otterize-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.otterize.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.otterize.registry }}/\
      {{ .Values.charts.otterize.repository }}"

  # openDesk Home
  # Source: https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home
  - name: "home-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.home.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.home.registry }}/\
      {{ .Values.charts.home.repository }}"

  # openDesk Certificates
  # Source: https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates
  - name: "certificates-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.certificates.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.certificates.registry }}/\
      {{ .Values.charts.certificates.repository }}"

  # openDesk PostgreSQL
  # Source: https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql
  - name: "postgresql-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.postgresql.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.postgresql.registry }}/\
      {{ .Values.charts.postgresql.repository }}"

  # openDesk MariaDB
  # Source: https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-mariadb
  - name: "mariadb-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.mariadb.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.mariadb.registry }}/\
      {{ .Values.charts.mariadb.repository }}"

  # openDesk Postfix
  # https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix
  - name: "postfix-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.postfix.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.postfix.registry }}/\
      {{ .Values.charts.postfix.repository }}"

  # openDesk ClamAV
  # https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
  - name: "clamav-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.clamav.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.clamav.registry }}/\
      {{ .Values.charts.clamav.repository }}"
  - name: "clamav-simple-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.clamavSimple.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.clamavSimple.registry }}/\
      {{ .Values.charts.clamavSimple.repository }}"

  # VMWare Bitnami
  # Source: https://github.com/bitnami/charts/
  - name: "memcached-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.memcached.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.memcached.registry }}/\
      {{ .Values.charts.memcached.repository }}"
  - name: "redis-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.redis.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.redis.registry }}/\
      {{ .Values.charts.redis.repository }}"
  - name: "minio-repo"
    keyring: "../../files/gpg-pubkeys/opencode.gpg"
    verify: {{ .Values.charts.minio.verify }}
    username: {{ env "OD_PRIVATE_REGISTRY_USERNAME" | quote }}
    password: {{ env "OD_PRIVATE_REGISTRY_PASSWORD" | quote }}
    oci: true
    url: "{{ .Values.global.helmRegistry | default .Values.charts.minio.registry }}/\
      {{ .Values.charts.minio.repository }}"

releases:
  - name: "opendesk-otterize"
    chart: "otterize-repo/{{ .Values.charts.otterize.name }}"
    version: "{{ .Values.charts.otterize.version }}"
    values:
      - "values-otterize.yaml.gotmpl"
    installed: {{ .Values.security.otterizeIntents.enabled }}
    timeout: 900

  - name: "opendesk-home"
    chart: "home-repo/{{ .Values.charts.home.name }}"
    version: "{{ .Values.charts.home.version }}"
    values:
      - "values-home.yaml.gotmpl"
    installed: {{ .Values.home.enabled }}

  - name: "opendesk-certificates"
    chart: "certificates-repo/{{ .Values.charts.certificates.name }}"
    version: "{{ .Values.charts.certificates.version }}"
    values:
      - "values-certificates.yaml.gotmpl"
    installed: {{ .Values.certificates.enabled }}
    timeout: 900

  - name: "redis"
    chart: "redis-repo/{{ .Values.charts.redis.name }}"
    version: "{{ .Values.charts.redis.version }}"
    values:
      - "values-redis.yaml.gotmpl"
    installed: {{ .Values.redis.enabled }}
    timeout: 900

  - name: "memcached"
    chart: "memcached-repo/{{ .Values.charts.memcached.name }}"
    version: "{{ .Values.charts.memcached.version }}"
    values:
      - "values-memcached.yaml.gotmpl"
    installed: {{ .Values.memcached.enabled }}
    timeout: 900

  - name: "postgresql"
    chart: "postgresql-repo/{{ .Values.charts.postgresql.name }}"
    version: "{{ .Values.charts.postgresql.version }}"
    values:
      - "values-postgresql.yaml.gotmpl"
    installed: {{ .Values.postgresql.enabled }}
    timeout: 900

  - name: "mariadb"
    chart: "mariadb-repo/{{ .Values.charts.mariadb.name }}"
    version: "{{ .Values.charts.mariadb.version }}"
    values:
      - "values-mariadb.yaml.gotmpl"
    installed: {{ .Values.mariadb.enabled }}
    timeout: 900

  - name: "postfix"
    chart: "postfix-repo/{{ .Values.charts.postfix.name }}"
    version: "{{ .Values.charts.postfix.version }}"
    values:
      - "values-postfix.yaml.gotmpl"
    installed: {{ .Values.postfix.enabled }}
    timeout: 900

  - name: "clamav"
    chart: "clamav-repo/{{ .Values.charts.clamav.name }}"
    version: "{{ .Values.charts.clamav.version }}"
    values:
      - "values-clamav-distributed.yaml.gotmpl"
    installed: {{ .Values.clamavDistributed.enabled }}
    timeout: 900

  - name: "clamav-simple"
    chart: "clamav-simple-repo/{{ .Values.charts.clamavSimple.name }}"
    version: "{{ .Values.charts.clamavSimple.version }}"
    values:
      - "values-clamav-simple.yaml.gotmpl"
    installed: {{ .Values.clamavSimple.enabled }}
    timeout: 900

  - name: "minio"
    chart: "minio-repo/{{ .Values.charts.minio.name }}"
    version: "{{ .Values.charts.minio.version }}"
    values:
      - "values-minio.yaml.gotmpl"
    installed: {{ .Values.minio.enabled }}
    timeout: 900

commonLabels:
  deploy-stage: "services"
  component: "services"
...
