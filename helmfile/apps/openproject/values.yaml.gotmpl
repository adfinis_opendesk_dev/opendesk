{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
global:
  imagePullSecrets:
    {{ .Values.global.imagePullSecrets | toYaml | nindent 4 }}

containerSecurityContext:
  enabled: true
  privileged: false
  runAsUser: 1000
  runAsGroup: 1000
  allowPrivilegeEscalation: false
  capabilities:
    drop:
      - "ALL"
  seccompProfile:
    type: "RuntimeDefault"
  readOnlyRootFilesystem: true
  runAsNonRoot: true
  seLinuxOptions:
    {{ .Values.seLinuxOptions.openproject | toYaml | nindent 4 }}

environment:
# For more details and more options see
# https://www.openproject.org/docs/installation-and-operations/configuration/environment/
  OPENPROJECT_LOG__LEVEL: {{ if .Values.debug.enabled }}"debug"{{ else }}"warn"{{ end }}
  OPENPROJECT_LOGIN__REQUIRED: "true"
  OPENPROJECT_OAUTH__ALLOW__REMAPPING__OF__EXISTING__USERS: "true"
  OPENPROJECT_OMNIAUTH__DIRECT__LOGIN__PROVIDER: "keycloak"
  OPENPROJECT_PER__PAGE__OPTIONS: "20, 50, 100, 200"
  OPENPROJECT_EMAIL__DELIVERY__METHOD: "smtp"
  OPENPROJECT_SMTP__AUTHENTICATION: "plain"
  OPENPROJECT_SMTP__ENABLE__STARTTLS__AUTO: "true"
  OPENPROJECT_SMTP__OPENSSL__VERIFY__MODE: "peer"
  OPENPROJECT_DEFAULT__COMMENT__SORT__ORDER: "desc"
  # Details: https://www.openproject-edge.com/docs/installation-and-operations/configuration/#seeding-ldap-connections
  OPENPROJECT_SEED_LDAP_OPENDESK_HOST: {{ .Values.ldap.host | quote }}
  OPENPROJECT_SEED_LDAP_OPENDESK_PORT: "389"
  OPENPROJECT_SEED_LDAP_OPENDESK_BINDPASSWORD: {{ .Values.secrets.univentionManagementStack.ldapSearch.openproject | quote }}
  OPENPROJECT_SEED_LDAP_OPENDESK_SECURITY: "plain_ldap"
  OPENPROJECT_SEED_LDAP_OPENDESK_BINDUSER: "uid=ldapsearch_openproject,cn=users,dc=swp-ldap,dc=internal"
  OPENPROJECT_SEED_LDAP_OPENDESK_BASEDN: "dc=swp-ldap,dc=internal"
  OPENPROJECT_SEED_LDAP_OPENDESK_FILTER:
    "(&(objectClass=opendeskProjectmanagementUser)(opendeskProjectmanagementEnabled=TRUE))"
  OPENPROJECT_SEED_LDAP_OPENDESK_SYNC__USERS: "true"
  OPENPROJECT_SEED_LDAP_OPENDESK_LOGIN__MAPPING: "uid"
  OPENPROJECT_SEED_LDAP_OPENDESK_FIRSTNAME__MAPPING: "givenName"
  OPENPROJECT_SEED_LDAP_OPENDESK_LASTNAME__MAPPING: "sn"
  OPENPROJECT_SEED_LDAP_OPENDESK_MAIL__MAPPING: "mailPrimaryAddress"
  OPENPROJECT_SEED_LDAP_OPENDESK_ADMIN__MAPPING: "opendeskProjectmanagementAdmin"
  OPENPROJECT_SEED_LDAP_OPENDESK_GROUPFILTER_OPENDESK_BASE: "dc=swp-ldap,dc=internal"
  OPENPROJECT_SEED_LDAP_OPENDESK_GROUPFILTER_OPENDESK_FILTER:
    "(&(objectClass=opendeskProjectmanagementGroup)(opendeskProjectmanagementEnabled=TRUE))"
  OPENPROJECT_SEED_LDAP_OPENDESK_GROUPFILTER_OPENDESK_SYNC__USERS: "true"
  OPENPROJECT_SEED_LDAP_OPENDESK_GROUPFILTER_OPENDESK_GROUP__ATTRIBUTE: "cn"
  OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_USER: {{ .Values.secrets.openproject.apiAdminUsername | quote }}
  OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_PASSWORD: {{ .Values.secrets.openproject.apiAdminPassword | quote }}
  OPENPROJECT_SOUVAP__NAVIGATION__SECRET: {{ .Values.secrets.centralnavigation.apiKey | quote }}
  OPENPROJECT_SOUVAP__NAVIGATION__URL: "https://{{ .Values.global.hosts.univentionManagementStack }}.{{ .Values.global.domain }}/univention/portal/navigation.json?base=https%3A//{{ .Values.global.hosts.univentionManagementStack }}.{{ .Values.global.domain }}"
  OPENPROJECT_SMTP__DOMAIN: {{ .Values.global.domain | quote }}
  OPENPROJECT_SMTP__USER__NAME: {{ .Values.smtp.username | quote }}
  OPENPROJECT_SMTP__PASSWORD: {{ .Values.smtp.password | quote }}
  OPENPROJECT_SMTP__PORT: {{ .Values.smtp.port | quote }}
  OPENPROJECT_SMTP__SSL: "false" # (default=false)
  OPENPROJECT_SMTP__ADDRESS: {{ .Values.smtp.host | quote }}
  OPENPROJECT_MAIL__FROM: "do-not-reply@{{ .Values.global.domain }}"
  OPENPROJECT_HOME__URL: {{ printf "https://%s.%s/" .Values.global.hosts.univentionManagementStack .Values.global.domain | quote }}
  OPENPROJECT_OPENID__CONNECT_KEYCLOAK_ISSUER: "https://{{ .Values.global.hosts.keycloak }}.{{ .Values.global.domain }}/realms/{{ .Values.platform.realm }}"
  OPENPROJECT_OPENID__CONNECT_KEYCLOAK_POST__LOGOUT__REDIRECT__URI: "https://{{ .Values.global.hosts.openproject }}.{{ .Values.global.domain }}/"

image:
  registry: {{ .Values.global.imageRegistry | default .Values.images.openproject.registry | quote }}
  repository: {{ .Values.images.openproject.repository | quote }}
  imagePullPolicy: {{ .Values.global.imagePullPolicy | quote }}
  tag: {{ .Values.images.openproject.tag | quote }}

initdb:
  image:
    registry: {{ .Values.global.imageRegistry | default .Values.images.openprojectInitDb.registry | quote }}
    repository: {{ .Values.images.openprojectInitDb.repository | quote }}
    tag: {{ .Values.images.openprojectInitDb.tag | quote }}
    imagePullPolicy: {{ .Values.global.imagePullPolicy | quote }}

memcached:
  bundled: false
  connection:
    host: {{ .Values.cache.openproject.host | quote }}
    port: {{ .Values.cache.openproject.port }}

persistence:
  enabled: false

postgresql:
  bundled: false
  auth:
    password: {{ .Values.databases.openproject.password | default .Values.secrets.postgresql.openprojectUser | quote }}
    username: {{ .Values.databases.openproject.username | quote }}
    database: {{ .Values.databases.openproject.name | quote }}
  connection:
    host: {{ .Values.databases.openproject.host | quote }}
    port: {{ .Values.databases.openproject.port }}

probes:
  liveness:
    initialDelaySeconds: 300
    failureThreshold: 30
  readiness:
    initialDelaySeconds: 150
    failureThreshold: 30

openproject:
  # seed will only be executed on initial installation
  seed_locale: "de"
  host: "{{ .Values.global.hosts.openproject }}.{{ .Values.global.domain }}"
  # Will only be set on initial seed / installation
  admin_user:
    name: "OpenProject Internal Admin"
    mail: "openproject-admin@swp-domain.internal"
    password_reset: "false"
    password: {{ .Values.secrets.openproject.adminPassword | quote }}
  oidc:
    enabled: true
    authorizationEndpoint: "/realms/{{ .Values.platform.realm }}/protocol/openid-connect/auth"
    endSessionEndpoint : "https://{{ .Values.global.hosts.keycloak }}.{{ .Values.global.domain }}/realms/{{ .Values.platform.realm }}/protocol/openid-connect/logout"
    host: "{{ .Values.global.hosts.keycloak }}.{{ .Values.global.domain }}"
    identifier: "opendesk-openproject"
    provider: "keycloak"
    scope: "[openid,opendesk]"
    secret: {{ .Values.secrets.keycloak.clientSecret.openproject | quote }}
    tokenEndpoint: "/realms/{{ .Values.platform.realm }}/protocol/openid-connect/token"
    userinfoEndpoint: "/realms/{{ .Values.platform.realm }}/protocol/openid-connect/userinfo"
    attribute_map:
      login: "opendesk_username"
      admin: "openproject_admin"
  useTmpVolumes: true

ingress:
  host: "{{ .Values.global.hosts.openproject }}.{{ .Values.global.domain }}"
  enabled: {{ .Values.ingress.enabled }}
  ingressClassName: {{ .Values.ingress.ingressClassName | quote }}
  tls:
    enabled: {{ .Values.ingress.tls.enabled }}
    secretName: {{ .Values.ingress.tls.secretName | quote }}

backgroundReplicaCount: {{ .Values.replicas.openprojectWorker }}

replicaCount: {{ .Values.replicas.openprojectWeb }}

resources:
  {{ .Values.resources.openproject | toYaml | nindent 2 }}

s3:
  enabled: true
  endpoint: {{ .Values.objectstores.openproject.endpoint | default (printf "https://%s.%s" .Values.global.hosts.minioApi .Values.global.domain) | quote }}
  host: {{ .Values.objectstores.openproject.endpoint | default (printf "https://%s.%s" .Values.global.hosts.minioApi .Values.global.domain) | quote }}
  pathStyle: {{ .Values.objectstores.openproject.pathStyle | quote }}
  region: {{ .Values.objectstores.openproject.region | quote }}
  bucketName: {{ .Values.objectstores.openproject.bucket | quote }}
  use_iam_profile: {{ .Values.objectstores.openproject.useIAMProfile | default "false" | quote }}
  auth:
    accessKeyId: {{ .Values.objectstores.openproject.username | quote }}
    secretAccessKey: {{ .Values.objectstores.openproject.secretKey | default .Values.secrets.minio.openprojectUser | quote }}

seederJob:
  annotations:
    intents.otterize.com/service-name: "openproject-seeder"

...
