# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
global:
  domain: {{ .Values.global.domain | quote }}
  hosts:
    {{ .Values.global.hosts | toYaml | nindent 4 }}
  imagePullSecrets:
    {{ .Values.global.imagePullSecrets | toYaml | nindent 4 }}

configuration:
  bot:
    username: "meetings-bot"
    displayname: "Terminplaner Bot"
  openxchangeBaseUrl: "https://{{ .Values.global.hosts.openxchange }}.{{ .Values.global.domain }}"
  strings:
    breakoutSessionWidgetName: "Breakoutsessions"
    calendarRoomName: "Terminplaner"
    calendarWidgetName: "Terminplaner"
    cockpitWidgetName: "Meeting Steuerung"
    jitsiWidgetName: "Videokonferenz"
    matrixNeoBoardWidgetName: "Whiteboard"
    matrixNeoChoiceWidgetName: "Abstimmungen"

containerSecurityContext:
  allowPrivilegeEscalation: false
  capabilities:
    drop:
      - "ALL"
  enabled: true
  privileged: false
  readOnlyRootFilesystem: true
  runAsGroup: 101
  runAsNonRoot: true
  runAsUser: 101
  seccompProfile:
    type: "RuntimeDefault"
  seLinuxOptions:
    {{ .Values.seLinuxOptions.matrixNeoDateFixBot | toYaml | nindent 4 }}

extraEnvVars:
  - name: "ACCESS_TOKEN"
    valueFrom:
      secretKeyRef:
        name: "matrix-neodatefix-bot-account"
        key: "access_token"

image:
  imagePullPolicy: {{ .Values.global.imagePullPolicy | quote }}
  registry: {{ .Values.global.imageRegistry | default .Values.images.matrixNeoDateFixBot.registry | quote }}
  repository: {{ .Values.images.matrixNeoDateFixBot.repository | quote }}
  tag: {{ .Values.images.matrixNeoDateFixBot.tag | quote }}

ingress:
  enabled: {{ .Values.ingress.enabled }}
  ingressClassName: {{ .Values.ingress.ingressClassName | quote }}
  tls:
    enabled: {{ .Values.ingress.tls.enabled }}
    secretName: {{ .Values.ingress.tls.secretName | quote }}

livenessProbe:
  enabled: true

persistence:
  size: {{ .Values.persistence.size.matrixNeoDateFixBot | quote }}
  storageClass: {{ .Values.persistence.storageClassNames.RWO | quote }}

podSecurityContext:
  enabled: true
  fsGroup: 101

readinessProbe:
  enabled: true

replicaCount: {{ .Values.replicas.matrixNeoDateFixBot }}

resources:
  {{ .Values.resources.matrixNeoDateFixBot | toYaml | nindent 2 }}

...
